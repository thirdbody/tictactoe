//
//  ViewController.swift
//  TicTacToe
//
//  Created by Milo Gilad on 1/28/19.
//  Copyright © 2019 NBPS. All rights reserved.
//

import UIKit

var grid = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0]
];

// true = Player 1; false = Player 2
var current = true;
var accessed: [UIButton] = [];

class ViewController: UIViewController {
    
    @IBOutlet weak var currentPlayerDisplay: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        currentPlayerDisplay.text = "Current: Player 1";
    }

    func victory() -> Bool {
        // check for straight-across
        for i in 0..<grid.count {
            // Sets can only have one of each value
            if (Set(grid[i]).count == 1) && (grid[i][0] != 0) {
                return true;
            }
        }
        
        // check for straight-vertical
        for i in 0...2 {
            if (grid[0][i], grid[1][i]) == (grid[1][i], grid[2][i]) && grid[1][i] != 0 {
                return true;
            }
        }
        
        // check for diagonal
        if (grid[0][0], grid[1][1]) == (grid[1][1], grid[2][2]) && grid[1][1] != 0 {
            return true;
        }
        if (grid[2][0], grid[1][1]) == (grid[1][1], grid[0][2]) && grid[1][1] != 0 {
            return true;
        }

        return false;
    }
    
    func resetGame() {
        grid = [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ];
        
        current = true;
        
        for button in accessed {
            button.setImage(nil, for: .normal);
        }
        
        accessed = [];
    }
    
    func stuck() -> Bool {
        for i in 0..<grid.count {
            for j in 1..<grid[i].count {
                if (grid[i][j] == 0) {
                    return false;
                }
            }
        }
        return true;
    }
    
    @IBAction func toggleTac(_ sender: UIButton) {
        // In the ID section of the button, put "row,col" under Accessibility -> ID (e.g. 1,2 = row 1, column 2)
        let location = sender.accessibilityIdentifier?.components(separatedBy: ",");
        
        // Forcing compatibility with array syntax
        let row = Int(location![0])! - 1;
        let col = Int(location![1])! - 1;
        
        // Do not take action if space has already been accessed
        if (grid[row][col] != 0) {
            return;
        }
        
        if (current) {
            grid[row][col] = 1;
            sender.setImage(UIImage(named: "x.png"), for: .normal);
        } else {
            grid[row][col] = 2;
            sender.setImage(UIImage(named: "o.png"), for: .normal);
        }
        
        accessed.append(sender);
        
        if (victory()) {
            currentPlayerDisplay.text = current ? "Player 1 Wins!" : "Player 2 Wins!";
            resetGame();
            return;
        } else if (stuck()) {
            currentPlayerDisplay.text = "Tie";
            resetGame();
            return;
        }
        
        current = !current;
        currentPlayerDisplay.text = current ? "Current: Player 1" : "Current: Player 2";
        
    }
    
}

